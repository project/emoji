<?php

namespace Drupal\emoji\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "emoji" plugin.
 *
 * @CKEditorPlugin(
 *   id = "emoji",
 *   label = @Translation("Emoji"),
 * )
 */
class Emoji extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/emoji/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return ['emoji_emojiListUrl' => '/libraries/emoji/emoji.json'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [
      'autocomplete',
      'textmatch',
      'ajax',
      'panelbutton',
      'floatpanel',
      'xml',
      'textwatcher',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'EmojiPanel' => [
        'label' => $this->t('Emoji'),
        'image' => 'libraries/emoji/icons/emojipanel.png',
      ],
    ];
  }

}
