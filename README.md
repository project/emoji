Installation
============

This module requires the core CKEditor module.

Download all the required libraries from https://ckeditor.com/cke4/addon/{library_name}.

  Ajax: https://ckeditor.com/cke4/addon/ajax -> Version: 4.11.4

  Autocomplete: https://ckeditor.com/cke4/addon/autocomplete -> Version: 4.11.4

  Panelbutton: https://ckeditor.com/cke4/addon/panelbutton -> Version: 4.11.4

  Floatpanel: https://ckeditor.com/cke4/addon/floatpanel -> Version: 4.11.4

  Textwatcher: https://ckeditor.com/cke4/addon/textwatcher -> Version 4.11.4

  Textmatch: https://ckeditor.com/cke4/addon/textmatch -> Version 4.11.4

  Xml: https://ckeditor.com/cke4/addon/xml -> Version 4.11.4

Place the libraries in the root libraries folder (/libraries).

Enable Emoji module in the Drupal admin.

LIBRARY INSTALLATION (COMPOSER)
-------------------------------
Copy the following into your project's composer.json file.
1. Ajax:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/ajax",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/ajax/releases/ajax_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

2.Autocomplete:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/autocomplete",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/autocomplete/releases/autocomplete_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```
3.Panelbutton:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/panelbutton",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/panelbutton/releases/panelbutton_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

4.Floatpanel:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/floatpanel",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/floatpanel/releases/floatpanel_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

5.Textwatcher:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/textwatcher",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/textwatcher/releases/textwatcher_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

6.Textmatch:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/textmatch",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/textmatch/releases/textmatch_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

7.Xml:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/xml",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/xml/releases/xml_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

8.Emoji:

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/emoji",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/emoji/releases/emoji_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```

9.Ensure you have following mapping inside your composer.json.
```
"extra": {
  "installer-paths": {
    "libraries/{$name}": ["type:drupal-library"]
  }
}
```
10.Run following command to download required libraries.
```
composer require ckeditor-plugin/ajax
composer require ckeditor-plugin/autocomplete
composer require ckeditor-plugin/panelbutton
composer require ckeditor-plugin/floatpanel
composer require ckeditor-plugin/textwatcher
composer require ckeditor-plugin/textmatch
composer require ckeditor-plugin/xml
composer require ckeditor-plugin/emoji
```
10.Enable the emoji module from Drupal UI or drush.
